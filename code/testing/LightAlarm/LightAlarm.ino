// ESP32 clock tutorial https://circuitdigest.com/microcontroller-projects/esp32-internet-clock

//ESP32 Wifi clock settings
#include <WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

const char* ssid     = "Darknet";                          //WiFi Name
const char* password = "92261630021405263095_zenner";                   // WiFi Password

#define NTP_OFFSET  3600 // In seconds
#define NTP_INTERVAL 60 * 1000    // In miliseconds
#define NTP_ADDRESS  "pool.ntp.org"

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, NTP_ADDRESS, NTP_OFFSET, NTP_INTERVAL);

#define alarmTime "00:15:00"
int alarmDuration = 60; // time in seconds
int alarmCounter = 0;
bool alarmRunning = false;


#define WHITEPIN 13
#define REDPIN 27
#define GREENPIN 14                  
#define BLUEPIN 12
int fader = 0;

     // make this higher to slow down

// setting PWM properties
const int freq = 5000;
const int whiteChannel = 0;
const int redChannel = 1;
const int greenChannel = 2;
const int blueChannel = 3;
const int resolution = 8;

void setup() {
  ledcSetup(whiteChannel, freq, resolution);
  ledcSetup(redChannel, freq, resolution);
  ledcSetup(greenChannel, freq, resolution);
  ledcSetup(blueChannel, freq, resolution);
  
  // attach the channel to the GPIO to be controlled
  ledcAttachPin(WHITEPIN, whiteChannel);
  ledcAttachPin(REDPIN, redChannel);
  ledcAttachPin(GREENPIN, greenChannel);
  ledcAttachPin(BLUEPIN, blueChannel);

  Serial.begin(9600);
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED){
  delay(500);
  Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  timeClient.begin();
}

void loop() {
  timeClient.update();
  String formattedTime = timeClient.getFormattedTime();
  // convert the String into int
  int hour = formattedTime.substring(0,2).toInt();
  int minute = formattedTime.substring(3,5).toInt();
  int second = formattedTime.substring(6,8).toInt();
  Serial.println(formattedTime);
  if (formattedTime == alarmTime){
    alarmRunning = true;
    Serial.println("Time reached");
  }
  if (alarmRunning && (alarmCounter <= alarmDuration)){
    alarmCounter = ++alarmCounter;
    Serial.println("Alarm!!");
    if (fader <= 245){
      fader = fader + 5;
    }
    ledcWrite(whiteChannel, fader);
    Serial.println(fader);
  } else {
    alarmRunning = false;
    fader = 0;
    ledcWrite(whiteChannel, fader);
  }
  delay(1000);
}
