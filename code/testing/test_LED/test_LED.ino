
#define WHITEPIN 13
#define REDPIN 27
#define GREENPIN 14
#define BLUEPIN 12
 
#define FADESPEED 10     // make this higher to slow down

// setting PWM properties
const int freq = 5000;
const int whiteChannel = 0;
const int redChannel = 1;
const int greenChannel = 2;
const int blueChannel = 3;
const int resolution = 8;
 
void setup() {
  ledcSetup(whiteChannel, freq, resolution);
  ledcSetup(redChannel, freq, resolution);
  ledcSetup(greenChannel, freq, resolution);
  ledcSetup(blueChannel, freq, resolution);
  
  // attach the channel to the GPIO to be controlled
  ledcAttachPin(WHITEPIN, whiteChannel);
  ledcAttachPin(REDPIN, redChannel);
  ledcAttachPin(GREENPIN, greenChannel);
  ledcAttachPin(BLUEPIN, blueChannel);
}
 
 
void loop() {
  int r, g, b, w;

 ledcWrite(redChannel, 0);
 ledcWrite(blueChannel, 0);
 ledcWrite(greenChannel, 0);
 ledcWrite(whiteChannel, 0);
 delay(1000);
 ledcWrite(redChannel, 255);
 delay(1000);
 ledcWrite(redChannel, 0);
 delay(1000);
 ledcWrite(blueChannel, 255);
 delay(1000);
 ledcWrite(blueChannel, 0);
 delay(1000);
 ledcWrite(greenChannel, 255);
 delay(1000);
 ledcWrite(greenChannel, 0);
 delay(1000);
 ledcWrite(whiteChannel, 255);
 delay(1000);
 ledcWrite(whiteChannel, 0);

 
  for (r = 0; r < 256; r++) { 
    ledcWrite(redChannel, r);
    delay(FADESPEED);
  } 
  for (r = 255; r > 0; r--) { 
    ledcWrite(redChannel, r);
    delay(FADESPEED);
  }
  

  for (g = 0; g < 256; g++) { 
    ledcWrite(greenChannel, g);
    delay(FADESPEED);
  } 
  // fade from yellow to green
  for (g = 255; g > 0; g--) { 
    ledcWrite(greenChannel, g);
    delay(FADESPEED);
  }


  for (b = 0; b < 256; b++) { 
    ledcWrite(blueChannel, b);
    delay(FADESPEED);
  } 
  for (b = 255; b > 0; b--) { 
    ledcWrite(blueChannel, b);
    delay(FADESPEED);
  }
  
    // white
  for (w = 0; g > 256; w++) { 
    ledcWrite(whiteChannel, w);
    delay(FADESPEED);
  }
  for (w = 255; g > 0; w--) { 
    ledcWrite(whiteChannel, w);
    delay(FADESPEED);
  } 
  
}
